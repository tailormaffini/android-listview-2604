package com.example.listclass;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class AnotacaoListAdapter extends ArrayAdapter<Anotacao> {

    private Context mContext;
    private int mResource;

    public AnotacaoListAdapter(Context context, int resource, List<Anotacao> objects) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource,parent,false);
        Anotacao anotacao = getItem(position);

        TextView titulo = (TextView) convertView.findViewById(R.id.textTitulo);
        TextView data = (TextView) convertView.findViewById(R.id.textData);
        ImageView image = (ImageView) convertView.findViewById(R.id.imageView);

        titulo.setText(anotacao.getTitulo());
        data.setText("Ultima edição: " + formatDateTime(anotacao.getDate()));
        image.setImageResource(R.drawable.bloco);


        return convertView;
    }

    public String formatDateTime(Date date) {

        String finalDateTime = "";


        if (date != null) {
            long when = date.getTime();
            int flags = 0;
            flags |= android.text.format.DateUtils.FORMAT_SHOW_TIME;
            flags |= android.text.format.DateUtils.FORMAT_SHOW_DATE;
            flags |= android.text.format.DateUtils.FORMAT_ABBREV_MONTH;
            flags |= android.text.format.DateUtils.FORMAT_SHOW_YEAR;

            finalDateTime = android.text.format.DateUtils.formatDateTime(this.getContext(),when + TimeZone.getDefault().getOffset(when), flags);
        }

        return finalDateTime;
    }
}
