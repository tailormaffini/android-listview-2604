package com.example.listclass;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static android.content.Context.MODE_PRIVATE;

public class AnotacaoDAO extends AppCompatActivity {
    ArrayList<Anotacao> anotacoes;
    SQLiteDatabase db;
    DateFormat dateFormat;

    public AnotacaoDAO(SQLiteDatabase db){
        this.anotacoes = new ArrayList();
        this.db = db;
        dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    }

    public ArrayList<Anotacao> getAnotacoesDoBanco(){
        Cursor cursor = db.rawQuery("SELECT * FROM anotacoes", null);
        cursor.moveToFirst();
        String dateTime;
        Date date = new Date();
        try{
            do{
                dateTime = cursor.getString(cursor.getColumnIndex("ultEdit"));
                try {
                    date = dateFormat.parse(dateTime);
                } catch (ParseException e) {}

                anotacoes.add(new Anotacao(cursor.getInt(cursor.getColumnIndex("id")),
                        cursor.getString(cursor.getColumnIndex("titulo")),
                        cursor.getString(cursor.getColumnIndex("conteudo")),
                        date));
            } while(cursor.moveToNext());
        } catch(Exception e){

        }

        return anotacoes;
    }

    public void novaAnotacao(Anotacao anotacao){
        db.execSQL("INSERT INTO anotacoes (titulo, conteudo, ultEdit)" +
                " VALUES ('" + anotacao.getTitulo() + "','" + anotacao.getConteudo() + "','" + getDataAtualFormatada() + "')");
    }
    public void updateAnotacao(Anotacao anotacao){
        db.execSQL("UPDATE anotacoes SET conteudo = '" + anotacao.getConteudo() + "', titulo = '" + anotacao.getTitulo() + "', ultEdit = '"+ getDataAtualFormatada() +"' WHERE id = " + anotacao.getCodigo());
    }
    public void apagarAnotacao(Anotacao anotacao){
        db.execSQL("DELETE FROM anotacoes WHERE id = " + anotacao.getCodigo());
    }

    public String getDataAtualFormatada(){
        Date date = Calendar.getInstance().getTime();
        return dateFormat.format(date);
    }
}
