package com.example.listclass;

import java.io.Serializable;
import java.util.Date;

public class Anotacao implements Serializable {
    private int codigo;
    private String titulo;
    private String conteudo;
    private Date date;

    public Anotacao(int codigo, String nome, String conteudo, Date date){
        this.codigo = codigo;
        this.titulo = nome;
        this.conteudo = conteudo;
        this.date = date;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getConteudo() {
        return conteudo;
    }

    public void setConteudo(String conteudo) {
        this.conteudo = conteudo;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }



}
