package com.example.listclass;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.Serializable;
import java.sql.Array;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    AnotacaoDAO anotDao;
    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = findViewById(R.id.listView);
        criarBancoEDao();

        Intent i = getIntent();
        if(i.hasExtra("anotacao")){
            Anotacao anot = (Anotacao) i.getSerializableExtra("anotacao");
            if(i.getIntExtra("excluir", 0) == 1){
                anotDao.apagarAnotacao(anot);
            } else {
                if(anot.getCodigo() == 0){
                    anotDao.novaAnotacao(anot);
                } else {
                    anotDao.updateAnotacao(anot);
                }
            }
        }


        AnotacaoListAdapter adapter = new AnotacaoListAdapter(this, R.layout.note, anotDao.getAnotacoesDoBanco());
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Anotacao selectedItem = (Anotacao) parent.getItemAtPosition(position);
                abrirAnotacao(selectedItem);
            }
        });
    }

    public void novaAnotacao(View view){
        abrirAnotacao(new Anotacao(0, "Nova anotação", "", new Date()));
    }

    public void abrirAnotacao(Anotacao anotacao){
        Intent intent = new Intent(this, AnotacaoView.class);
        intent.putExtra("anotacao", anotacao);

        startActivity(intent);

    }

    public void criarBancoEDao(){
        db = openOrCreateDatabase("Banco", MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS anotacoes(id Integer PRIMARY KEY AUTOINCREMENT," +
                "titulo VARCHAR," +
                "conteudo VARCHAR," +
                "ultEdit DATETIME)");

        anotDao = new AnotacaoDAO(db);
    }

}
