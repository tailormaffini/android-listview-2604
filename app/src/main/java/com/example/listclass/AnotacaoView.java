package com.example.listclass;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AnotacaoView extends AppCompatActivity {

    int id;
    Anotacao anotacao;
    EditText titulo, conteudo;
    Button btnExcluir;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anotacao_view);
        Intent intent = getIntent();
        anotacao = (Anotacao) intent.getSerializableExtra("anotacao");

        titulo = findViewById(R.id.etTitulo);
        conteudo = findViewById(R.id.etConteudo);
        btnExcluir = findViewById(R.id.button3);

        titulo.setText(anotacao.getTitulo());
        conteudo.setText(anotacao.getConteudo());
        if(anotacao.getCodigo() == 0){
            btnExcluir.setClickable(false);
            btnExcluir.setEnabled(false);
        }
    }

    public void salvarAnotacao(View view){
        anotacao.setConteudo(conteudo.getText().toString());
        anotacao.setTitulo(titulo.getText().toString());

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("anotacao", anotacao);
        intent.putExtra("excluir", 0);
        startActivity(intent);
    }

    public void excluirAnotacao(View view){
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("anotacao", anotacao);
        intent.putExtra("excluir", 1);
        startActivity(intent);
    }
}
